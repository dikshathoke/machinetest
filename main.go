package main

import (
	"Machinetest/model"

	"github.com/joho/godotenv"
)

func main() {

	// Load env file
	godotenv.Load()
	// connect to database
	model.ConnectDatabase()

	// Routes 

}
