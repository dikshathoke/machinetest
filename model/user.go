package model

import (
	"time"
)

type User struct {
	ID         int       `json:"id"`
	User_Name  string    `json:"user_name"`
	Created_at time.Time `json:"created_at"`
}
