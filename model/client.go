package model

import (
	"time"
)

type Client struct {
	ID          int       `json:"id"`
	Client_Name string    `json:"client_name"`
	Created_at  time.Time `json:"created_at"`
	Created_by  int       `json:"created_by"`
	User        User      `gorm:foreignkey:Created_by" json:"-"`
	ProjectID   int
	Project     Project
}
