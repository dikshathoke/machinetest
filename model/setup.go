package model

import (
	"fmt"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

type Settings struct {
	DB_HOST     string
	DB_NAME     string
	DB_USER     string
	DB_PASSWORD string
	DB_PORT     string
}

func InitializeSettings() Settings {

	DB_HOST := os.Getenv("DB_HOST")
	DB_NAME := os.Getenv("DB_NAME")
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")
	DB_PORT := os.Getenv("DB_PORT")

	switch {
	case DB_HOST == "":
		fmt.Println("Environment Variable DB_Host not set.")
		os.Exit(1)

	case DB_NAME == "":
		fmt.Println("Environment Variable DB_Name not set.")
		os.Exit(1)

	case DB_USER == "":
		fmt.Println("Environment Variable DB_User not set.")
		os.Exit(1)

	case DB_PASSWORD == "":
		fmt.Println("Environment Variable DB_Password not set.")
		os.Exit(1)

	case DB_PORT == "":
		fmt.Println("Environment  Variable DB_Port not set.")
		os.Exit(1)
	}

	settings := Settings{
		DB_HOST:     DB_HOST,
		DB_NAME:     DB_NAME,
		DB_USER:     DB_USER,
		DB_PASSWORD: DB_PASSWORD,
		DB_PORT:     DB_PORT,
	}
	return settings
}

func ConnectDatabase() {

	settings := InitializeSettings()
	dsn := "host=" + settings.DB_HOST + " user=" + settings.DB_USER + " password=" + settings.DB_PASSWORD + " dbname=" + settings.DB_NAME + " port=" + settings.DB_PORT + " sslmode=disable TimeZone=Asia/Kolkata"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Failed to connect database")
	}

	db.AutoMigrate(&Client{}, &Project{}, &User{})
	DB = db

}
