package model

import (
	"time"
)

type Project struct {
	ID         int       `json:"id"`
	Created_at time.Time `json:"created_at"`
	Created_by int       `json:"created_by"`
}
